using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Inicio : MonoBehaviour
{
    public GameObject pantalla1, instruciones, boton2,boton1;
    
    // Start is called before the first frame update
    void Start()
    {
        pantalla1.gameObject.SetActive(true);
        boton1.gameObject.SetActive(true);
        instruciones.gameObject.SetActive(false);
        boton2.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   public void Activar()
    {       
        pantalla1.gameObject.SetActive(false);
        boton1.gameObject.SetActive(false);
        instruciones.gameObject.SetActive(true);
        boton2.gameObject.SetActive(true);
    }

   public void Comenzar()
    {
        SceneManager.LoadScene("1");
    }
}
